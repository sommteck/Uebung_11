using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uebung_11
{
    public partial class stringfunktionen : Form
    {
        // Daklaration der Funktion für die Zeit für den auskommentierten Code-Teil
        // in der Methode 'cmd_split_time_Click'.
        //
        //DateTime date = new DateTime();

        public stringfunktionen()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_10_character_Click(object sender, EventArgs e)
        {
            string teilEins = null, teilZwei = null;

            txt_output.Text = null;

            if(txt_input.TextLength < 10)
            {
                teilEins = txt_input.Text;
                teilZwei = "---";
            }
            else
            {
                teilEins = txt_input.Text.Substring(0, 10);
                teilZwei = txt_input.Text.Substring(10);
            }

            txt_output.AppendText("Erster Stringteil: " + teilEins + "\r\nZweiter Stringteil: " + teilZwei);
        }

        private void cmd_split_string_Click(object sender, EventArgs e)
        {
            string[] s = txt_input.Text.Split('s');

            txt_output.Text = null;

            for (int i = 0; i < s.Length; i = i + 1)
            {
                txt_output.AppendText("Teilstring " + Convert.ToString(i + 1) + ": " + s[i] + "\r\n");
            }
        }

        private void cmd_copy_Click(object sender, EventArgs e)
        {
            // Zeichenarray deklarieren mit Länge des Eingabetextes
            char[] zeichen = new char[txt_input.Text.Length];

            // Länge des Eingabetextes ausgeben
            txt_output.Text = Convert.ToString(txt_input.Text.Length);

            // Eingabetext zeichenweise in Zeichenarray kopieren
            for (int i = 0; i <= txt_input.Text.Length - 1; i = i + 1)
            {
                txt_input.Text.CopyTo(i , zeichen, i , 1);
                MessageBox.Show(Convert.ToString(i));
            }

            txt_output.Text += "\r\n";
    
            foreach (char c in zeichen)
            {
                txt_output.Text += c;
            }
        }

        private void cmd_split_folder_Click(object sender, EventArgs e)
        {

            try
            {
                string[] path = null;

                txt_output.Text = null;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog1.FileName.Split('\\');
                }

                txt_output.AppendText("\r\n");

                foreach (string s in path)
                {
                    txt_output.AppendText(s + "\r\n");
                }
            }

            catch
            {

            }
        }

        private void cmd_split_time_Click(object sender, EventArgs e)
        {
            char[] Zerteiler = { '.', ' ', ':' };
            string zeit = Convert.ToString(DateTime.Now);
            string[] zeichenstrang = zeit.Split(Zerteiler);

            for (int i = 0; i <= 5; i = i + 1)
            {
                switch (i)
                {
                    case 0:
                        txt_time.AppendText("Tag:\t" + zeichenstrang[i]);
                        break;
                    case 1:
                        txt_time.AppendText("Monat:\t" + zeichenstrang[i]);
                        break;
                    case 2:
                        txt_time.AppendText("Jahr:\t" + zeichenstrang[i]);
                        break;
                    case 3:
                        txt_time.AppendText("Stunde:\t" + zeichenstrang[i]);
                        break;
                    case 4:
                        txt_time.AppendText("Minute:\t" + zeichenstrang[i]);
                        break;
                    case 5:
                        txt_time.AppendText("Sekunde:\t" + zeichenstrang[i]);
                        break;
                }

                txt_time.AppendText("\r\n");
            }

            /*
            string LocalTime = "";
            
            int startIndex = 0;
            int length_year = 4;
            int length_mounth = 3;
            int length_day = 3;
            int length_hour = 2;
            int length_minutes = 2;
            int length_seconds = 2;

            date = DateTime.Now;
            txt_time.Text = "Vollständige Anzeige: " + date.ToLocalTime() + "\r\n";
            txt_time.Text += "\r\n";
            txt_time.Text += "Gesplittet:\r\n";

            LocalTime = Convert.ToString(date.ToLocalTime());
            txt_time.Text += "Tag: " + LocalTime.Substring(startIndex, length_day) + "\r\n";
            txt_time.Text += "Monat: " + LocalTime.Substring(startIndex + 3, length_mounth) + "\r\n";
            txt_time.Text += "Jahr: " + LocalTime.Substring(startIndex + 6, length_year) + "\r\n";
            txt_time.Text += "Stunde: " + LocalTime.Substring(startIndex + 11, length_hour) + "\r\n";
            txt_time.Text += "Minuten: " + LocalTime.Substring(startIndex + 14, length_minutes) + "\r\n";
            txt_time.Text += "Sekunden : " + LocalTime.Substring(startIndex + 17, length_seconds) + "\r\n";
            */
        }
    }
}