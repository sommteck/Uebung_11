namespace Uebung_11
{
    partial class stringfunktionen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_input = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.Zeit = new System.Windows.Forms.Label();
            this.txt_time = new System.Windows.Forms.TextBox();
            this.cmd_end = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmd_copy = new System.Windows.Forms.Button();
            this.cmd_split_string = new System.Windows.Forms.Button();
            this.cmd_10_character = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmd_split_folder = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmd_split_time = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input";
            // 
            // txt_input
            // 
            this.txt_input.Location = new System.Drawing.Point(12, 32);
            this.txt_input.Multiline = true;
            this.txt_input.Name = "txt_input";
            this.txt_input.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_input.Size = new System.Drawing.Size(397, 104);
            this.txt_input.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Output";
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(12, 167);
            this.txt_output.Multiline = true;
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_output.Size = new System.Drawing.Size(397, 122);
            this.txt_output.TabIndex = 3;
            // 
            // Zeit
            // 
            this.Zeit.AutoSize = true;
            this.Zeit.Location = new System.Drawing.Point(13, 306);
            this.Zeit.Name = "Zeit";
            this.Zeit.Size = new System.Drawing.Size(30, 16);
            this.Zeit.TabIndex = 4;
            this.Zeit.Text = "Zeit";
            // 
            // txt_time
            // 
            this.txt_time.Location = new System.Drawing.Point(12, 325);
            this.txt_time.Multiline = true;
            this.txt_time.Name = "txt_time";
            this.txt_time.ReadOnly = true;
            this.txt_time.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_time.Size = new System.Drawing.Size(397, 99);
            this.txt_time.TabIndex = 5;
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(482, 32);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(89, 26);
            this.cmd_end.TabIndex = 6;
            this.cmd_end.Text = "Exit";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmd_copy);
            this.groupBox1.Controls.Add(this.cmd_split_string);
            this.groupBox1.Controls.Add(this.cmd_10_character);
            this.groupBox1.Location = new System.Drawing.Point(464, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(120, 150);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Strings";
            // 
            // cmd_copy
            // 
            this.cmd_copy.Location = new System.Drawing.Point(18, 107);
            this.cmd_copy.Name = "cmd_copy";
            this.cmd_copy.Size = new System.Drawing.Size(89, 27);
            this.cmd_copy.TabIndex = 2;
            this.cmd_copy.Text = "Kopieren";
            this.cmd_copy.UseVisualStyleBackColor = true;
            this.cmd_copy.Click += new System.EventHandler(this.cmd_copy_Click);
            // 
            // cmd_split_string
            // 
            this.cmd_split_string.Location = new System.Drawing.Point(18, 64);
            this.cmd_split_string.Name = "cmd_split_string";
            this.cmd_split_string.Size = new System.Drawing.Size(89, 27);
            this.cmd_split_string.TabIndex = 1;
            this.cmd_split_string.Text = "Teilen";
            this.cmd_split_string.UseVisualStyleBackColor = true;
            this.cmd_split_string.Click += new System.EventHandler(this.cmd_split_string_Click);
            // 
            // cmd_10_character
            // 
            this.cmd_10_character.Location = new System.Drawing.Point(18, 22);
            this.cmd_10_character.Name = "cmd_10_character";
            this.cmd_10_character.Size = new System.Drawing.Size(89, 30);
            this.cmd_10_character.TabIndex = 0;
            this.cmd_10_character.Text = "10 Zeichen";
            this.cmd_10_character.UseVisualStyleBackColor = true;
            this.cmd_10_character.Click += new System.EventHandler(this.cmd_10_character_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmd_split_folder);
            this.groupBox2.Location = new System.Drawing.Point(464, 251);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(120, 59);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ordner";
            // 
            // cmd_split_folder
            // 
            this.cmd_split_folder.Location = new System.Drawing.Point(18, 22);
            this.cmd_split_folder.Name = "cmd_split_folder";
            this.cmd_split_folder.Size = new System.Drawing.Size(89, 30);
            this.cmd_split_folder.TabIndex = 0;
            this.cmd_split_folder.Text = "Teilen";
            this.cmd_split_folder.UseVisualStyleBackColor = true;
            this.cmd_split_folder.Click += new System.EventHandler(this.cmd_split_folder_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmd_split_time);
            this.groupBox3.Location = new System.Drawing.Point(464, 325);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(120, 64);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zeit";
            // 
            // cmd_split_time
            // 
            this.cmd_split_time.Location = new System.Drawing.Point(18, 22);
            this.cmd_split_time.Name = "cmd_split_time";
            this.cmd_split_time.Size = new System.Drawing.Size(89, 27);
            this.cmd_split_time.TabIndex = 0;
            this.cmd_split_time.Text = "Teilen";
            this.cmd_split_time.UseVisualStyleBackColor = true;
            this.cmd_split_time.Click += new System.EventHandler(this.cmd_split_time_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // stringfunktionen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 436);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.txt_time);
            this.Controls.Add(this.Zeit);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_input);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "stringfunktionen";
            this.Text = "Stringfunktionen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Label Zeit;
        private System.Windows.Forms.TextBox txt_time;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmd_copy;
        private System.Windows.Forms.Button cmd_split_string;
        private System.Windows.Forms.Button cmd_10_character;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cmd_split_folder;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button cmd_split_time;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}